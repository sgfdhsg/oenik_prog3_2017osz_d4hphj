﻿// <copyright file="EnemyShip.cs" company="Oszlánszky Csaba">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FF_Oszlanszky_Csaba_d4hphj
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Media;

    /// <summary>
    /// The EnemyShip class
    /// </summary>
    /// <value>
    /// Az ellenséges hajó geometriáját illetve tulajdonságait
    /// </value>
    public class EnemyShip : GameObject
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EnemyShip"/> class.
        /// </summary>
        /// <param name="centerX">Az ellenséges hajó középppontjának X koordinátája</param>
        /// <param name="centerY">Az ellenséges hajó középpontjának Y koordinájtája</param>
        /// <value>
        /// Ez a konkstruktor inicializálja az ellenséges hajót, itt történik meg a geometriájának a meghatározása
        /// </value>
        public EnemyShip(int centerX, int centerY)
        {
            this.CenterX = centerX;
            this.CenterY = centerY;

            Point frontPipePoint = new Point(centerX - 25, centerY);
            Point rearPipePoint = new Point(centerX - 15, centerY);
            Point frontTrianglePoint = rearPipePoint;
            Point rearTrianglePoint1 = new Point(centerX + 25, centerY - 25);
            Point rearTrianglePoint2 = new Point(centerX + 25, centerY + 25);
            this.EnemyShipFrontX = centerX - 25;
            this.EnemyShipFrontY = centerY;

            GeometryGroup myShipGroup = new GeometryGroup();
            myShipGroup.Children.Add(new LineGeometry(frontTrianglePoint, rearTrianglePoint1));
            myShipGroup.Children.Add(new LineGeometry(frontTrianglePoint, rearTrianglePoint2));
            myShipGroup.Children.Add(new RectangleGeometry(new Rect(rearTrianglePoint1.X, rearTrianglePoint1.Y - 15, 25, 80)));

            myShipGroup.Children.Add(new LineGeometry(frontPipePoint, rearPipePoint));
            Geometry myShipGeotmetry = myShipGroup.GetWidenedPathGeometry(new Pen(Brushes.DarkCyan, 3));
            this.Geometry = myShipGeotmetry;
        }

        /// <summary>
        /// Gets the EnemyShipFrontX field
        /// </summary>
        /// <value>
        /// Az ellenséges hajó elejének az X koordinátája
        /// </value>
        public int EnemyShipFrontX { get; private set; }

        /// <summary>
        /// Gets the EnemyShipFrontY field
        /// </summary>
        /// <value>
        /// Az ellenséges hajó elejének az Y koordinátája
        /// </value>
        public int EnemyShipFrontY { get; private set; }

        /// <summary>
        /// Gets or sets the CenterY field
        /// </summary>
        /// <value>
        /// Az ellenséges hajó közepének az Y koordinátája
        /// </value>
        private int CenterY { get; set; }

        /// <summary>
        /// Gets or sets the CenterX field
        /// </summary>
        /// <value>
        /// Az ellenséges hajó közepének az Y koordinátája
        /// </value>
        private int CenterX { get; set; }
    }
}
