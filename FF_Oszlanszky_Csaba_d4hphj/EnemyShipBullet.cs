﻿// <copyright file="EnemyShipBullet.cs" company="Oszlánszky Csaba">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FF_Oszlanszky_Csaba_d4hphj
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Media;

    /// <summary>
    /// The EnemyShipBullet class
    /// </summary>
    /// <value>
    /// Az ellenséges hajó lövedékeihez tartozó tulajdonságokat, illetve a geometriáját tartalmazza
    /// </value>
    public class EnemyShipBullet : GameObject
    {
        /// <summary>
        /// The frontPoint field
        /// </summary>
        private Point frontPoint;

        /// <summary>
        /// The endPoint field
        /// </summary>
        private Point endPoint;

        /// <summary>
        /// Initializes a new instance of the <see cref="EnemyShipBullet" /> class.
        /// </summary>
        /// <param name="shipFrontX"> Az ellenség hajó elejáének X koordinátája</param>
        /// <param name="shipFrontY"> Az ellenség hajó elejáének Y koordinátája</param>
        /// <param name="maxTurns"> A lövedék élettartama</param>
        /// <value>
        /// Elvégzi a lövedék létrehozását, amely az adott ellenséges hajóhoz tartozik, illetve létrehozza a geometriáját
        /// </value>
        public EnemyShipBullet(int shipFrontX, int shipFrontY, int maxTurns)
        {
            this.MaxTurns = maxTurns;
            this.FrontPoint = new Point(shipFrontX, shipFrontY);
            this.EndPoint = new Point(shipFrontX + 20, shipFrontY);
            LineGeometry enemyBulletGeometry = new LineGeometry(this.FrontPoint, this.EndPoint);
            Geometry geometry = enemyBulletGeometry.GetWidenedPathGeometry(new Pen(Brushes.Red, 2));
            this.Geometry = geometry;
        }

        /// <summary>
        /// Gets the Turns property
        /// </summary>
        /// <value>
        /// A körök száma.
        /// </value>
        public int Turns { get; private set; }

        /// <summary>
        /// Gets the MaxTurns property
        /// </summary>
        /// <value>
        /// A maximális körök száma.
        /// </value>
        public int MaxTurns { get; private set; }

        /// <summary>
        /// Gets or sets the FrontPoint property
        /// </summary>
        /// <value>
        /// Az első pontja a lövedéknek
        /// </value>
        public Point FrontPoint
        {
            get { return this.frontPoint; }
            set { this.frontPoint = value; }
        }

        /// <summary>
        /// Gets or sets the EndPoint property
        /// </summary>
        /// <value>
        /// A hátsó pontja a lövedéknek.
        /// </value>
        public Point EndPoint
        {
            get { return this.endPoint; }
            set { this.endPoint = value; }
        }
    }
}
