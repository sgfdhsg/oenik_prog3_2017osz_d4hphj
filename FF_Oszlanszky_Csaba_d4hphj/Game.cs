﻿// <copyright file="Game.cs" company="Oszlánszky Csaba">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FF_Oszlanszky_Csaba_d4hphj
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;

    /// <summary>
    /// The MoveDirection enum
    /// </summary>
    /// <value>
    /// Ez az enum tartalmazza a mozgások irányát, fel vagy pedig le
    /// </value>
    public enum MoveDirection
    {
        /// <summary>
        /// Fel
        /// </summary>
        Up,

        /// <summary>
        /// Le
        /// </summary>
        Down
    }

    /// <summary>
    /// The Game class
    /// </summary>
    /// <value>
    /// Ez az osztály valósítja meg a teljes játék logikát
    /// </value>
    public class Game
    {
        private int turnsForShips;
        private int turnsSinceLastEnemyShip;
        private int enemyBulletSpeed;
        private int myBulletSpeed;
        private int counter;

        /// <summary>
        /// Initializes a new instance of the <see cref="Game" /> class.
        /// </summary>
        /// <param name="screenWidth"> Az adott ablak szélessége</param>
        /// <param name="screenHeight">Az adott ablak magassága</param>
        /// <value>
        /// Inicializálja a Game osztályt, beállítja a megfelelő kezdőértékeket
        /// </value>
        public Game(int screenWidth, int screenHeight)
        {
            this.ScreenHeight = screenHeight;
            this.ScreenWidth = screenWidth;

            this.MyShip = new MyShip(100, 100);

            this.EnemyShips = new List<EnemyShip>();
            this.EnemyBullets = new List<EnemyShipBullet>();

            this.EnemyShips.Add(new EnemyShip(screenWidth - 80, screenHeight - 80));
            this.EnemyBullets.Add(new EnemyShipBullet(screenWidth - 110, screenHeight - 80, 100));
            this.MyBullets = new List<MyShipBullet>();
            this.MyBulletSpeed = 5;
            this.EnemyBulletSpeed = 5;
            this.TurnsForShips = 150;
        }

        /// <summary>
        /// Gets or sets the MyShip property
        /// </summary>
        /// <value>
        /// Saját hajó
        /// </value>
        public MyShip MyShip { get; set; }

        /// <summary>
        /// Gets or sets the EnemyShip list
        /// </summary>
        /// <value>
        /// Az ellenséghajók listája
        /// </value>
        public List<EnemyShip> EnemyShips { get; set; }

        /// <summary>
        /// Gets or sets the EnemyShipBullet list
        /// </summary>
        /// <value>
        /// Az ellengség hajóinak a lövedékeinek a listája
        /// </value>
        public List<EnemyShipBullet> EnemyBullets { get; set; }

        /// <summary>
        /// Gets or sets the MyShipBullet list
        /// </summary>
        /// <value>
        /// A saját hajónak a lövedékeinek a listája
        /// </value>
        public List<MyShipBullet> MyBullets { get;  set; }

        /// <summary>
        /// Gets or sets the Counter
        /// </summary>
        /// <value>
        /// A számoló
        /// </value>
        public int Counter
        {
            get { return this.counter; }
            set { this.counter = value; }
        }

        /// <summary>
        /// Gets or sets the screenWidth
        /// </summary>
        /// <value>
        /// Ablakszélesség
        /// </value>
        private int ScreenWidth { get; set; }

        /// <summary>
        /// Gets or sets the screenHeight
        /// </summary>
        /// <value>
        /// Ablakmagasság
        /// </value>
        private int ScreenHeight { get; set; }

        /// <summary>
        /// Gets or sets the turnsForShips
        /// </summary>
        /// <value>
        /// A hajók köreinek a száma
        /// </value>
        private int TurnsForShips
        {
            get { return this.turnsForShips; }
            set { this.turnsForShips = value; }
        }

        /// <summary>
        /// Gets or sets the turnsSinceLastEnemyShip
        /// </summary>
        /// <value>
        /// Az utolsó ellenséges hajó megjelenése óta eltelt körök száma
        /// </value>
        private int TurnsSinceLastEnemyShip
        {
            get { return this.turnsSinceLastEnemyShip; }
            set { this.turnsSinceLastEnemyShip = value; }
        }

        /// <summary>
        /// Gets or sets the enemyBulletSpeed
        /// </summary>
        /// <value>
        /// Az ellenséges hajó lövedékének a sebessége
        /// </value>
        private int EnemyBulletSpeed
        {
            get { return this.enemyBulletSpeed; }
            set { this.enemyBulletSpeed = 5; }
        }

        /// <summary>
        /// Gets or sets the myBulletSpeed
        /// </summary>
        /// <value> A saját hajó lövedékének a sebessége</value>
        private int MyBulletSpeed
        {
            get { return this.myBulletSpeed; }
            set { this.myBulletSpeed = value; }
        }

        /// <summary>
        /// The DoTurn method
        /// </summary>
        /// <returns> Azt az igaz vagy hamis állítást adja vissza,
        /// hogy a saját hajó meghalt-e vagy pedig lehet még egy
        /// játékkört megcsinálni
        /// </returns>
        /// <value>
        /// Létrehozza a törlendő ellenséges hajók listáját
        /// Létrehozza a törlendő ellenséges hajók lövedékének listáját
        /// Létrehozza a törlendő saját hajó lövedékeinek listáját
        /// Minden egyes ellenséges hajó esetében végig megy az összes
        /// létező ellenséges hajó lövedéken, és elvégzi a lövedék mozgatását,
        /// illetve megnézi, hogy az ellenséges hajó lövedéke nem találkozott-e,
        /// a saját hajóval.
        /// Amennyiben saját hajó lövedéke találkozott az ellenséges hajóval,
        /// akkor az ellenséges hajót áthelyezi a törlendő hajók listájába,
        /// ezzel egyidőben pedig a lövedéket ami eltalálta is áthelyezi a törlendő lövedékek listájába
        /// Megnézi, hogy mennyi kör telt el az utolsó ellenséges hajó létrejötte óta,
        /// és amennyiben egyezik a megadott értékkel,
        /// amennyinként kell új ellenséges hajót létrehoznia,
        /// akkor elvégzi azt, majd pedig egy lövést is.
        /// Elvégzi az ellenséges hajó lövedékének a sebességének a növelését,
        /// illetve kiüríti a törlendő hajók, lövedékek listáit.
        /// Majd pedig visszadja, hogy eltalálta-e ellenséges lövedék a saját hajót, vagy pedig nem.
        /// </value>
        public bool DoTurn()
        {
            bool end = false;
            List<EnemyShip> enemyShipsToDelete = new List<EnemyShip>();
            List<EnemyShipBullet> enemyBulletsToDelete = new List<EnemyShipBullet>();
            List<MyShipBullet> myShipBulletsToDelete = new List<MyShipBullet>();

            foreach (EnemyShip ship in this.EnemyShips)
            {
                foreach (EnemyShipBullet bullet in this.EnemyBullets)
                {
                    bullet.Location = new Point(bullet.Location.X - this.enemyBulletSpeed, bullet.Location.Y);

                    if (bullet.CollidesWith(this.MyShip))
                    {
                        end = true;
                    }

                    if (bullet.Location.X > 0)
                    {
                        enemyBulletsToDelete.Add(bullet);
                    }
                }

                foreach (MyShipBullet myBullet in this.MyBullets)
                {
                    myBullet.Location = new Point(myBullet.Location.X + this.myBulletSpeed, myBullet.Location.Y);
                    if (myBullet.CollidesWith(ship))
                    {
                        enemyShipsToDelete.Add(ship);
                        myShipBulletsToDelete.Add(myBullet);
                        this.counter++;
                    }
                }
            }

            foreach (EnemyShip enemyShip in enemyShipsToDelete)
            {
                this.EnemyShips.Remove(enemyShip);
            }

            foreach (MyShipBullet myBullet in myShipBulletsToDelete)
            {
                this.MyBullets.Remove(myBullet);
            }

            this.TurnsSinceLastEnemyShip++;

            if (this.TurnsSinceLastEnemyShip == this.TurnsForShips)
            {
                Random rand = new Random();
                int random = rand.Next(0, this.ScreenHeight - 30);
                while (random < 25)
                {
                    random = rand.Next(0, this.ScreenHeight - 30);
                }

                this.EnemyShips.Add(new EnemyShip(this.ScreenWidth - 80, this.ScreenHeight - random));
                this.EnemyBullets.Add(new EnemyShipBullet(this.ScreenWidth - 80, this.ScreenHeight - random, 100));
                this.TurnsSinceLastEnemyShip = 0;
                this.EnemyBulletSpeed++;
                enemyShipsToDelete.Clear();
                myShipBulletsToDelete.Clear();
                if (!(this.TurnsForShips <= 50))
                {
                    this.TurnsForShips -= 10;
                }
            }

            return end;
        }

        /// <summary>
        /// The MyShipMove method
        /// </summary>
        /// <param name="direction">A mozgás iránya, le vagy pedig fel</param>
        /// <value>
        /// Megnézi, hogy a paraméter a fel vagy pedig a le értékkel egyezik,
        /// majd pedig ennek megfelelően elvégzi a hajó mozgatását
        /// </value>
        public void MyShipMove(MoveDirection direction)
         {
            if (direction == MoveDirection.Up && this.MyShip.Location.Y >= -50)
            {
                this.MyShip.Move(-5);
                this.MyShip.Location = new Point(this.MyShip.Location.X, this.MyShip.Location.Y - 5);
            }
            else if (direction == MoveDirection.Down && this.MyShip.Location.Y <= this.ScreenHeight - 150)
            {
                this.MyShip.Move(+5);
                this.MyShip.Location = new Point(this.MyShip.Location.X, this.MyShip.Location.Y + 5);
            }
        }

        /// <summary>
        /// The Shoot method
        /// </summary>
        /// <value>
        /// A saját hajó elejáhez ad egy lövedéket, melyet elhelyez a saját hajó lövedékek listában
        /// </value>
        public void Shoot()
        {
            this.MyBullets.Add(new MyShipBullet((int)this.MyShip.Location.X + 115, (int)this.MyShip.Location.Y + 100, 100));
        }
    }
}