﻿// <copyright file="GameFrameworkElement.cs" company="Oszlánszky Csaba">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FF_Oszlanszky_Csaba_d4hphj
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Media;
    using System.Windows.Threading;

    /// <summary>
    /// The GameFrameworkElement class
    /// </summary>
    /// <value>
    /// Ez az osztály valósítja meg azt, hogy pontosan hogyan is kell történnie a megjelenitésnek.
    /// </value>
    public class GameFrameworkElement : FrameworkElement
    {
        /// <summary>
        /// The game field
        /// </summary>
        private Game game;

        /// <summary>
        /// The timer field
        /// </summary>
        private DispatcherTimer timer;

        /// <summary>
        /// Initializes a new instance of the <see cref="GameFrameworkElement" /> class.
        /// </summary>
        /// <value>
        /// Feliratkozik a konstruktor 2 eseményre, a Loaded és a KeyDown eseményre.
        /// </value>
        public GameFrameworkElement()
        {
            this.Loaded += this.GameFrameworkElement_Loaded;
            this.KeyDown += this.GameFrameworkElement_KeyDown;
        }

        /// <summary>
        /// The KeyDown event
        /// </summary>
        /// <param name="sender">The sender parameter</param>
        /// <param name="e">The e parameter</param>
        /// <value>
        /// Ez a esemény nézi, hogy melyik billentyű van lenyomva.
        /// Amennyiben megfelel a feltételnek, akkor elvégzi a saját hajó mozgatását,
        /// vagy pedig létrehoz egy új saját hajó lövedéket.
        /// </value>
        protected void GameFrameworkElement_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.Up)
            {
                this.game.MyShipMove(MoveDirection.Up);
            }
            else if (e.Key == System.Windows.Input.Key.Down)
            {
                this.game.MyShipMove(MoveDirection.Down);
            }
            else if (e.Key == System.Windows.Input.Key.Space)
            {
                this.game.Shoot();
            }

            this.InvalidateVisual();
        }

        /// <summary>
        /// The Loaded event
        /// </summary>
        /// <param name="sender">The sender parameter</param>
        /// <param name="e">The e parameter</param>
        /// <value>
        /// Ez az esemény elvégzi az ablak betöltésekor az alábbiakat:
        /// Létrehozza a game-t
        /// Értesíti a megjelenítést változásról
        /// Fokuszálhatóvá teszi, és fokuszálja az ablakot
        /// Elindítja a timer-t 20 ms-os időközzel
        /// Feliratkozik a Timer_Tick eseményre
        /// </value>
        protected void GameFrameworkElement_Loaded(object sender, RoutedEventArgs e)
        {
            if (DesignerProperties.GetIsInDesignMode(this) == false)
            {
                this.game = new Game((int)this.ActualWidth, (int)this.ActualHeight);
                this.InvalidateVisual();

                this.Focusable = true;
                this.Focus();

                this.timer = new DispatcherTimer();
                this.timer.Interval = new TimeSpan(0, 0, 0, 0, 20);
                this.timer.Tick += this.Timer_Tick;
                this.timer.Start();
            }
        }

        /// <summary>
        /// The Timer_Tick event
        /// </summary>
        /// <param name="sender">The sender parameter</param>
        /// <param name="e">The e parameter</param>
        /// <value>
        /// Minden egyes ütésre(tick) megnézi, hogy a game DoTurn metódusa, igaz vagy hamis értéket adott-e vissza.
        /// Amennyiben igaz (tehát a saját hajót eltalálta egy ellenséges hajó lövedéke)
        /// akkor a játéknak vége van, leállítja a timert
        /// </value>
        protected void Timer_Tick(object sender, EventArgs e)
        {
            bool end = this.game.DoTurn();
            if (end)
            {
                this.timer.Stop();
                MessageBox.Show("Sajnos vesztettél :(  \nEnnyi hajót lőttél le: " + this.game.Counter);
                Window.GetWindow(this).Close();
            }

            this.InvalidateVisual();
        }

        /// <summary>
        /// The Onrender method
        /// </summary>
        /// <param name="drawingContext"> The drawingXoncetext parameter</param>
        /// <value>
        /// Ez a metódus valósítja meg a kirajzolását az egyes elemeknek, amennyiben a game mezőnk nem üres.
        /// </value>
        protected override void OnRender(DrawingContext drawingContext)
        {
            base.OnRender(drawingContext);

            if (this.game != null)
            {
                drawingContext.DrawGeometry(Brushes.OrangeRed, new Pen(Brushes.Moccasin, 1), this.game.MyShip.GetTransformedGeometry());
                foreach (EnemyShip ship in this.game.EnemyShips)
                {
                    drawingContext.DrawGeometry(Brushes.Red, null, ship.Geometry);
                }

                foreach (EnemyShipBullet enemyShipBullet in this.game.EnemyBullets)
                {
                    drawingContext.DrawGeometry(Brushes.PaleVioletRed, null, enemyShipBullet.GetTransformedGeometry());
                }

                foreach (MyShipBullet myShipBullet in this.game.MyBullets)
                {
                    drawingContext.DrawGeometry(Brushes.Green, new Pen(Brushes.BlueViolet, 3), myShipBullet.GetTransformedGeometry());
                }
            }
        }
    }
}
