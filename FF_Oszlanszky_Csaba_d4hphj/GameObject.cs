﻿// <copyright file="GameObject.cs" company="Oszlánszky Csaba">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FF_Oszlanszky_Csaba_d4hphj
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Media;

    /// <summary>
    /// The GameObject class
    /// </summary>
    /// <value> Minden olyan dolgot tartalmaz, amelyet az egyes leszármazottak,
    /// az enemyShip, enemyShipBullet, myShip, illetve myShipBullet osztályok
    /// egyaránt használnak.
    /// Ilyen a Geometry, a Point, a terület,
    /// a mozgás, illetve az ütközés
    /// </value>
    public class GameObject
    {
        /// <summary>
        /// Gets or sets the Location property
        /// </summary>
        public Point Location { get; set; }

        /// <summary>
        /// Gets or sets the Geometry property
        /// </summary>
        public Geometry Geometry { get; set; }

        /// <summary>
        /// Gets the Realarea property
        /// </summary>
        /// <value>
        /// Szükséges az ütközés vizsgálathoz
        /// </value>
        public Geometry RealArea
        {
            get
            {
                TransformGroup tg = new TransformGroup();
                tg.Children.Add(new TranslateTransform(this.Location.X, this.Location.Y));
                this.Geometry.Transform = tg;
                return this.Geometry.GetFlattenedPathGeometry();
            }
        }

        /// <summary>
        /// The GetTransformedGeometry method
        /// </summary>
        /// <returns>
        /// Geometriát ad vissza, melyet a GameFrameworkElement tud érzelmezni és az OnRender kirajzolni
        /// </returns>
        public Geometry GetTransformedGeometry()
        {
            Geometry copy = this.Geometry.Clone();
            copy.Transform = new TranslateTransform(this.Location.X, this.Location.Y);
            return copy.GetFlattenedPathGeometry();
        }

        /// <summary>
        /// The CollidesWith method
        /// </summary>
        /// <param name="other"> Egy másik GameObject elem</param>
        /// <returns>
        /// Visszaadja, hogy igaz-e, hogy a két GameObject elem területe nagyobb-e mint 1
        /// </returns>
        public bool CollidesWith(GameObject other)
        {
            bool ret = Geometry.Combine(this.RealArea, other.RealArea, GeometryCombineMode.Intersect, null).GetArea() > 0;
            return ret;
        }
    }
}
