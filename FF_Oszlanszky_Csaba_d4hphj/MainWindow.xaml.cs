﻿// <copyright file="MainWindow.xaml.cs" company="Oszlánszky Csaba">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FF_Oszlanszky_Csaba_d4hphj
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Navigation;
    using System.Windows.Shapes;

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MainWindow" /> class.
        /// </summary>
        public MainWindow()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// The Startbutton_Click event
        /// </summary>
        /// <param name="sender">The sender parameter</param>
        /// <param name="e">The e parameter</param>
        /// <value>
        /// Az esemény bekövetkezésének a hatására nyit egy új GameWindow ablakot
        /// </value>
        private void StartButton_Click(object sender, RoutedEventArgs e)
        {
            GameWindow gameWindow = new GameWindow();
            gameWindow.ShowDialog();
        }

        /// <summary>
        /// The ExitButton_Click event
        /// </summary>
        /// <param name="sender">The sender parameter</param>
        /// <param name="e">The e parameter</param>
        /// <value>
        /// Az esemény bekövetkezésének hatására bezárja az ablakot.
        /// </value>
        private void ExitButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
