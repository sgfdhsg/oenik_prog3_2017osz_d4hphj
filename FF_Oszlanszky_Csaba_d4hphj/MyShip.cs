﻿// <copyright file="MyShip.cs" company="Oszlánszky Csaba">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FF_Oszlanszky_Csaba_d4hphj
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Media;

    /// <summary>
    /// The MyShip class
    /// </summary>
    public class MyShip : GameObject
    {
        /// <summary>
        /// The centerX field
        /// </summary>
        /// <value>
        /// A saját hajó középpontjának az X koordinátája
        /// </value>
        private int centerX;

        /// <summary>
        /// The centerY field
        /// </summary>
        /// <value>
        /// A saját hajó középpontjának az Y koordinátája
        /// </value>
        private int centerY;

        /// <summary>
        /// Initializes a new instance of the <see cref="MyShip" /> class.
        /// </summary>
        /// <param name="centerX">A sahát hajó középpontjának X koordinátája</param>
        /// <param name="centerY">A saját hajó középpontjának Y koordinátája</param>
        /// <value>
        /// Ez a konkstruktor inicializálja az saját hajót, itt történik meg a geometriájának a meghatározása
        /// </value>
        public MyShip(int centerX, int centerY)
        {
            this.centerX = centerX;
            this.centerY = centerY;

            Point frontPipePoint = new Point(centerX + 25, centerY);
            Point rearPipePoint = new Point(centerX + 15, centerY);
            Point frontTrianglePoint = rearPipePoint;
            Point rearTrianglePoint1 = new Point(centerX - 25, centerY - 25);
            Point rearTrianglePoint2 = new Point(centerX - 25, centerY + 25);

            GeometryGroup myShipGroup = new GeometryGroup();
            myShipGroup.Children.Add(new LineGeometry(frontTrianglePoint, rearTrianglePoint1));
            myShipGroup.Children.Add(new LineGeometry(frontTrianglePoint, rearTrianglePoint2));
            myShipGroup.Children.Add(new RectangleGeometry(new Rect(rearTrianglePoint1.X - 25, rearTrianglePoint1.Y - 15, 25, 80)));

            myShipGroup.Children.Add(new LineGeometry(frontPipePoint, rearPipePoint));
            Geometry myShipGeotmetry = myShipGroup.GetWidenedPathGeometry(new Pen(Brushes.DarkCyan, 2));
            this.Geometry = myShipGeotmetry;
}

        /// <summary>
        /// The Move method
        /// </summary>
        /// <param name="dy">Ennyivel toljuk el mozgáskor a hajót</param>
        /// <value>
        /// A Move metodus valósítja meg a középpont változtatását a függőleges tengelyen, aminek hatására a saját hajó mozogni fog.
        /// </value>
        public void Move(int dy)
        {
            this.centerY = this.centerY + dy;
        }
    }
}
