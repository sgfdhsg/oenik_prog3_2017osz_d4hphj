﻿// <copyright file="MyShipBullet.cs" company="Oszlánszky Csaba">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FF_Oszlanszky_Csaba_d4hphj
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Media;

    /// <summary>
    /// The MyShipBullet class
    /// </summary>
    /// <value>
    /// Ez az osztály valósítja meg a saját hajó lövedékének a geometriáját,
    /// illetve az életét, vagyis, hogy menyni körig él az adott lövedék
    /// </value>
    public class MyShipBullet : GameObject
    {
        /// <summary>
        /// The frontPoint field
        /// </summary>
        private Point frontPoint;

        /// <summary>
        /// The endPoint field
        /// </summary>
        private Point endPoint;

        /// <summary>
        /// Initializes a new instance of the <see cref="MyShipBullet" /> class.
        /// </summary>
        /// <param name="shipFrontX"> A saját hajó elejének az X koordinátája</param>
        /// <param name="shipFrontY"> A saját hajó elejének az Y koordinátája</param>
        /// <param name="maxTurns"> A lövedék max körszáma</param>
        /// <value>
        /// A konstruktor inicializálja a MyShipBullet osztályt,
        /// és meghatározza a geometriáját
        /// </value>
        public MyShipBullet(int shipFrontX, int shipFrontY, int maxTurns)
        {
            this.MaxTurns = maxTurns;
            this.FrontPoint = new Point(shipFrontX, shipFrontY);
            this.EndPoint = new Point(shipFrontX + 10, shipFrontY);
            LineGeometry myBulletGeometry = new LineGeometry(this.FrontPoint, this.EndPoint);
            Geometry geometry = myBulletGeometry.GetWidenedPathGeometry(new Pen(Brushes.Green, 2));
            this.Geometry = geometry;
        }

        /// <summary>
        /// Gets the Turns property
        /// </summary>
        /// <value> Körök száma</value>
        public int Turns { get; private set; }

        /// <summary>
        /// Gets the MaxTurns property
        /// </summary>
        /// <value> Maximális körök száma</value>
        public int MaxTurns { get; private set; }

        /// <summary>
        /// Gets or sets the FrontPoint property
        /// </summary>
        /// <value> A kezdő pontja a lövedéknek</value>
        public Point FrontPoint
        {
            get { return this.frontPoint; }
            set { this.frontPoint = value; }
        }

        /// <summary>
        /// Gets or sets the EndPoint property
        /// </summary>
        /// <value> A végpontja a lövedéknek</value>
        public Point EndPoint
        {
            get { return this.endPoint; }
            set { this.endPoint = value; }
        }
    }
}
